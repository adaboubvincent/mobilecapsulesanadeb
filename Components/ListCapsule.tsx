import * as React from 'react';
import { View, Text, FlatList, ActivityIndicator, StyleSheet } from 'react-native';

import { getAllCapsules } from '../APICall/APISpaceX';
import { IProps, IState } from '../Interfaces/Types';
import CapsuleItem from './CapsuleItem';

class  ListCapsule extends React.Component<IProps, IState> {
  //Component pour representer la liste des capsules

  constructor(props: any){
    super(props);
    this.state = {
      capsules: [],
      isLoading: false
    }
  }

  componentDidMount(){
    //Methode appelant lors de la representation du component
    //Cette methode nous permet ici de charger la liste des capsules
    this.setState({
      isLoading: true
    });
    
      getAllCapsules().then((data) => {
      this.setState({
        capsules: [...data],
        isLoading: false
      });
      });
      
  }


  _loadingWidget(){
    //Cette methode nous permet de representer la barre de progression lors du chargement des capsules
    if(this.state.isLoading){
      //Si isLoading est à true
        return(
            <View style={[styles.container_loading, styles.horizontal]}>
                <ActivityIndicator size="large" color="#00ff00" />
          </View>
        );
    }
  }
  
  _viewsDetailCapsule = (capsule: any) => {
    //Cette methode nous permet de naviger vers la page de detail d'une capsule
    this.props.navigation.navigate("Detail de la Capsule", {capsule: capsule});
  }

  render(){
    
    return(
      <View style={{ flex: 1, marginTop: 8 }}>
        { this._loadingWidget() }
        <FlatList
            data={this.state.capsules}
            keyExtractor={(item) => item.capsule_serial.toString()}
            onEndReachedThreshold={0.5}
            onEndReached={() => {
                
            }}
            renderItem={
              ({item}) => <CapsuleItem capsule={item} detailCap={this._viewsDetailCapsule} />
            }
        />
      </View>
    )
  }
}


const styles = StyleSheet.create({
    container_loading: {
      flex: 1,
      justifyContent: "center"
    },
    horizontal: {
      flexDirection: "row",
      justifyContent: "space-around",
      padding: 10
    }
  });

export default ListCapsule;